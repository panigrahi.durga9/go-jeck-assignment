//
//  Go_Jeck_AssignmentUITests.swift
//  Go-Jeck AssignmentUITests
//
//  Created by Muvi on 3/18/19.
//  Copyright © 2019 Durga. All rights reserved.
//

import XCTest
import UIKit

class Go_Jeck_AssignmentUITests: XCTestCase {
   
    //var app = ContactListCell()
        
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        
        let app = XCUIApplication()
        //change the text with respect to the 1st name of the tableview cell. Other wise it show test failed.
        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Just Chill "]/*[[".cells.staticTexts[\"Just Chill \"]",".staticTexts[\"Just Chill \"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        let favouriteButtonButton = app.buttons["favourite button"]
        favouriteButtonButton.tap()
        
        let okButton = app.alerts["Success!"].buttons["Ok"]
        okButton.tap()
        favouriteButtonButton.tap()
        okButton.tap()
        
        let masterNavigationBar = app.navigationBars["Master"]
        masterNavigationBar.buttons["Edit"].tap()
        
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let textField = element.children(matching: .other).element(boundBy: 1).children(matching: .textField).element
        textField.tap()
        
        let textField2 = element.children(matching: .other).element(boundBy: 2).children(matching: .textField).element
        textField2.tap()
        
        let textField3 = element.children(matching: .other).element(boundBy: 3).children(matching: .textField).element
        textField3.tap()
        
        let textField4 = element.children(matching: .other).element(boundBy: 4).children(matching: .textField).element
        textField4.tap()
        app.navigationBars["Edit"].buttons["Done"].tap()
        okButton.tap()
        masterNavigationBar.buttons["Contact"].tap()
        app.navigationBars["Contact"].buttons["Add"].tap()
        textField.tap()
        textField2.tap()
        textField3.tap()
        textField4.tap()
        app.navigationBars["Add"].buttons["Done"].tap()
        okButton.tap()
       
            
        
    }

}
