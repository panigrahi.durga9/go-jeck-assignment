//
//  ContactListCell.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 17/03/19.
//  Copyright © 2019 Durga. All rights reserved.
//

import UIKit

public class ContactListCell: UITableViewCell {

    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var favoriteImage: UIImageView!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.contactName.textColor = .textColor
        self.contactImage.layer.cornerRadius = self.contactImage.frame.height / 2
        self.contactImage.clipsToBounds = true
        self.contactImage.contentMode = .scaleAspectFit
        
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    var contactModel: ContactsViewModel!{
    didSet {
    self.contactName.text = contactModel.first_name
      
    self.contactImage.kf.setImage(with: URL(string: contactModel.profile_pic), placeholder: UIImage(), options: nil, progressBlock: nil) { (Image, error, cache, url) in
   }
    self.favoriteImage.image = (contactModel.favorite ) ? UIImage(named: "homeFavorite") : UIImage()
        }
    }
    

    
}
