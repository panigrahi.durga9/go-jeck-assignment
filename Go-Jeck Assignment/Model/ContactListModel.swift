//
//  ContactListModel.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 17/03/19.
//  Copyright © 2019 Durga. All rights reserved.
//

import Foundation

struct ContactList: Decodable{
    let id: Int?
    let first_name: String?
    let last_name: String?
    var profile_pic: String?
    let favorite: Bool?
    let url: String?
}
