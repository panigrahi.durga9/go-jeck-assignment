//
//  ContactDetailsModel.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 16/03/19.
//  Copyright © 2019 Muvi. All rights reserved.
//

import Foundation
struct ContactDetails: Decodable{
    let id: Int?
    let first_name: String?
    let last_name: String?
    let email: String?
    let phone_number: String?
    var profile_pic: String?
    let favorite: Bool?
    let created_at: String?
    let updated_at: String?
}
