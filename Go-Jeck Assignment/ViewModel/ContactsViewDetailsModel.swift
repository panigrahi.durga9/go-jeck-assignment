//
//  ContactsViewModel.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 3/17/19.
//  Copyright © 2019 Durga. All rights reserved.
//

import Foundation
import UIKit
struct ContactsViewDetailsModel {
    let id: Int?
    let first_name: String?
    let last_name: String?
    let email: String?
    let phone_number: String?
    var profile_pic: String?
    let favorite: Bool?
    let created_at: String?
    let updated_at: String?
    let name:String?

    init(contact:ContactDetails) {
        self.id = contact.id ?? 0
        self.first_name = contact.first_name ?? ""
        self.last_name = contact.last_name ?? ""
        self.name = self.first_name! + " " + self.last_name!
        self.phone_number = contact.phone_number ?? ""
        self.email = contact.email ?? ""
        self.created_at = contact.created_at ?? ""
        self.updated_at = contact.updated_at ?? ""
        
        var pic = contact.profile_pic
        if pic!.contains("/images/missing.png"){
            pic = "http://gojek-contacts-app.herokuapp.com/images/missing.png"

        }
        self.profile_pic = pic ?? ""
        self.favorite = contact.favorite ?? false
       
        
        
       
    }
}
