//
//  ContactsViewModel.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 3/17/19.
//  Copyright © 2019 Durga. All rights reserved.
//

import Foundation
import UIKit
struct ContactsViewModel {
    let id: Int
    let first_name: String
    let profile_pic: String
    let favorite: Bool
    let url: String

    init(contact:ContactList) {
        self.id = contact.id ?? 0
        self.first_name = (contact.first_name ?? "") + " " + (contact.last_name ?? "")
        
        var pic = contact.profile_pic
        if pic!.contains("/images/missing.png"){
            pic = "http://gojek-contacts-app.herokuapp.com/images/missing.png"

        }
        self.profile_pic = pic ?? ""
        self.favorite = contact.favorite ?? false
        self.url = contact.url ?? ""
        
        
       
    }
}
