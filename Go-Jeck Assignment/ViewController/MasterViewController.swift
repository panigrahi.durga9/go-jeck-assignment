//
//  MasterViewController.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 3/17/19.
//  Copyright © 2019 Durga. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {
    var detailViewController: DetailViewController? = nil
    var objects = [Any]()
    var objectC = Controller()
    var appD = UIApplication.shared.delegate as! AppDelegate
    var tableViewIndex = [String]()
    var allContacts = [ContactsViewModel]()
    var progressHUD = ProgressHUD(text: "Load Data...")
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorColor = .tableSeparatorColor
        self.tableView.register(UINib(nibName: "ContactListCell", bundle: nil), forCellReuseIdentifier: "ContactListCell")
        let groupButton = UIBarButtonItem(title: "Groups", style: .plain, target: self, action: #selector(insertNewContact(_:)))
        groupButton.tintColor = .buttonColor
        groupButton.title = "Groups"
        navigationItem.leftBarButtonItem = groupButton
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewContact(_:)))
        navigationItem.rightBarButtonItem = addButton
        navigationItem.rightBarButtonItem?.tintColor = .buttonColor
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        self.tableView.sectionIndexColor = .tableSeparatorColor
      
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        
        // Create and add the view to the screen.
    
        self.view.addSubview(progressHUD)
        // All done!
        
        self.fetchContactList()
        super.viewWillAppear(animated)
    }
    
    @objc
    func insertNewContact(_ sender: Any) {
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddEditViewController") as! AddEditViewController
        destination.addEdit = "Add"
        let navigationBar = UINavigationController(rootViewController: destination)
        self.present(navigationBar, animated: false, completion: nil)
        
    }
    func fetchContactList(){
        self.objectC.getContactList(url: self.appD.appBaseURL +  "contacts.json") { (succeeded: Bool, contacts: [ContactList]) -> () in
            
            DispatchQueue.main.async(execute: {
                
                self.progressHUD.removeFromSuperview()
                if(!succeeded){
                    
                }
                else
                {
                    self.allContacts = contacts.map({ return ContactsViewModel(contact: $0)})
                    self.tableViewIndex.sort()
                    self.tableView.reloadData()
                }
            })
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.allContacts.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell", for: indexPath) as! ContactListCell
        
        cell.contactModel = self.allContacts[indexPath.row]
        //cell.contactName.accessibilityIdentifier = "ContactName"

        
        return cell
    }
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return tableViewIndex
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        destination.currentContactIndex = self.allContacts[indexPath.row].id
        self.navigationController?.pushViewController(destination, animated: true)
    }

    

}
// MARK: - Remove Duplicates from an Array
extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
