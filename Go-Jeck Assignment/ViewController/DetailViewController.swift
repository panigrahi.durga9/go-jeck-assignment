//
//  DetailViewController.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 3/17/19.
//  Copyright © 2019 Durga. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var detailDescriptionLabel: UILabel!
    var currentContactIndex = 0
    var objectC = Controller()
    var progressHUD = ProgressHUD(text: "Load Data...")
    var appD = UIApplication.shared.delegate as! AppDelegate
    var profile_pic = ""
    var fName = ""
    var lName = ""
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var mobileLabelName: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var mobileBottomView: UIView!
    
    @IBOutlet weak var emailBottomView: UIView!
    @IBOutlet weak var emailLabelName: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = ""
        self.navigationController?.navigationBar.tintColor = .buttonColor
        
        let editButton = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(self.editAction))
        self.navigationItem.rightBarButtonItem = editButton
        
        self.mobileLabel.text = ""
        self.emailLabel.text = ""
        
        self.mobileLabel.textColor = .textColor
        self.mobileLabelName.textColor = .headerColor
        self.mobileBottomView.backgroundColor = .tableSeparatorColor
        
        self.emailLabel.textColor = .textColor
        self.emailLabelName.textColor = .headerColor
        self.emailBottomView.backgroundColor = .tableSeparatorColor
        
        self.favoriteButton.setImage(UIImage(named: "favourite_button"), for: .normal)
        self.favoriteButton.setImage(UIImage(named: "favourite_button_selected"), for: .selected)
        
    
    }
    
    @IBAction func fevUnFevBtn(_ sender: Any) {
       self.view.addSubview(progressHUD)
        
        let params: [String: String]
        if(favoriteButton.isSelected){
             params = ["favorite": "false"]
        }
        else{
             params = ["favorite": "true"]
        }
        
        
        self.objectC.editContactDetails(url: self.appD.appBaseURL +  "contacts/\(currentContactIndex).json", params: params) { (succeeded: Bool, contactDetails: ContactDetails) -> () in
            
            DispatchQueue.main.async(execute: {
                
               self.progressHUD.removeFromSuperview()
                if(!succeeded){
                    
                }
                else
                {
                    print(contactDetails)
                    let alertController = UIAlertController(title:"Success!", message: "Successfully Update.", preferredStyle: .alert)
                    
                    
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                       
                        self.favoriteButton.isSelected = contactDetails.favorite ?? false
                    }
                    
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.topView.createGradientLayer()
        self.contactImage.layer.cornerRadius = self.contactImage.frame.height / 2
        self.contactImage.layer.borderColor = UIColor.contactImageBorderColor.cgColor
        self.contactImage.layer.borderWidth = 3
        self.contactImage.layer.masksToBounds = true
        self.fetchContact()
    }
    func fetchContact(){
        self.view.addSubview(progressHUD)
        self.objectC.getContactDetails(url: self.appD.appBaseURL +  "contacts/\(currentContactIndex).json") { (succeeded: Bool, contactDetails: ContactDetails) -> () in
            
            DispatchQueue.main.async(execute: {
                
                 self.progressHUD.removeFromSuperview()
                if(!succeeded){
                    
                }
                else
                {
                    print(contactDetails)
                    let allContacts = ContactsViewDetailsModel.init(contact: contactDetails)
                    self.profile_pic = allContacts.profile_pic ?? ""
                    self.nameLabel.text = allContacts.name ?? ""
                    self.fName = allContacts.first_name ?? ""
                    self.lName = allContacts.last_name ?? ""
                    self.contactImage.kf.setImage(with: URL(string: allContacts.profile_pic ?? ""), placeholder: UIImage(named: "placeholder_photo"), options: nil, progressBlock: nil, completionHandler: nil)
                    self.mobileLabel.text = allContacts.phone_number ?? ""
                    self.emailLabel.text = allContacts.email ?? ""
                    self.favoriteButton.isSelected = allContacts.favorite ?? false
                }
            })
        }
    }
    @objc func editAction(){
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddEditViewController") as! AddEditViewController
        destination.addEdit = "Edit"
        destination.firstNameTxt = self.fName
        destination.lastNameTxt = self.lName
        destination.emailTxt = self.emailLabel.text ?? ""
        destination.phoneTxt = self.mobileLabel.text ?? ""
        destination.profile_pic = self.profile_pic
        destination.fev = self.favoriteButton.isSelected
         destination.currentContactIndex = self.currentContactIndex
        let navigationBar = UINavigationController(rootViewController: destination)
        self.present(navigationBar, animated: false, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
