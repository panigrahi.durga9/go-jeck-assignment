//
//  AddEditViewController.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 3/17/19.
//  Copyright © 2019 Durga. All rights reserved.
//

import UIKit

class AddEditViewController: UIViewController {

    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var emailId: UITextField!
    var appD = UIApplication.shared.delegate as! AppDelegate
    var progressHUD = ProgressHUD(text: "Load Data...")
    var objectC = Controller()
    var firstNameTxt = ""
    var lastNameTxt = ""
    var phoneTxt = ""
    var emailTxt = ""
    var profile_pic = ""
    var addEdit = ""
    var fev = false
    var currentContactIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = addEdit
        firstName.text = firstNameTxt
        lastName.text = lastNameTxt
        phone.text = phoneTxt
        emailId.text = emailTxt
        
        
       
        if (self.profile_pic.contains("/images/missing.png")) || (self.profile_pic == ""){
            self.profile_pic = (self.appD.appBaseURL + "images/missing.png")
        }
       
        self.profilePic.kf.setImage(with: URL(string: self.profile_pic), placeholder: UIImage(named: "placeholder_photo"), options: nil, progressBlock: nil, completionHandler: nil)
        
        
        let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.cancelAction))
        self.navigationItem.leftBarButtonItem = cancelBarButton
        
        if addEdit == "Add"{
            let doneBarButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.addAction))
            self.navigationItem.rightBarButtonItem = doneBarButton
        }
        else{
            let doneBarButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.editData))
            self.navigationItem.rightBarButtonItem = doneBarButton
        }
        
        
        self.navigationController?.navigationBar.tintColor = .buttonColor
        
    }
    @objc func cancelAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func addAction(){
        self.view.addSubview(progressHUD)
        
        let params: [String: String] = [ "first_name": self.firstName.text ?? "","last_name": self.lastName.text ?? "","email":self.emailId.text ?? "","phone_number": self.phone.text ?? "","favorite": "false"]
        
        self.objectC.addContactDetails(url: "http://gojek-contacts-app.herokuapp.com/contacts.json", params: params) { (succeeded: Bool, contactDetails: ContactDetails) -> () in
            
            DispatchQueue.main.async(execute: {
                 self.progressHUD.removeFromSuperview()
               
                if(!succeeded){
                    
                }
                else
                {
                    print(contactDetails)
                    let alertController = UIAlertController(title: UserDefaults.standard.string(forKey: "alert") ?? "Success!", message: "Successfully Added.", preferredStyle: .alert)
                    
                    
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.dismiss(animated: true, completion: nil)
                       
                    }
                    
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            })
        }
    }
    
    @objc func editData(){
        self.view.addSubview(progressHUD)
        
        let params: [String: String] = [ "first_name": self.firstName.text ?? "","last_name": self.lastName.text ?? "","email":self.emailId.text ?? "","phone_number": self.phone.text ?? "","favorite": String(fev)]
        
        self.objectC.editContactDetails(url: self.appD.appBaseURL +  "contacts/\(currentContactIndex).json", params: params) { (succeeded: Bool, contactDetails: ContactDetails) -> () in
            
            DispatchQueue.main.async(execute: {
                
                 self.progressHUD.removeFromSuperview()
                if(!succeeded){
                    
                }
                else
                {
                    print(contactDetails)
                    let alertController = UIAlertController(title: UserDefaults.standard.string(forKey: "alert") ?? "Success!", message: "Successfully Update.", preferredStyle: .alert)
                    
                    
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.dismiss(animated: true, completion: nil)
                        
                    }
                    
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            })
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.topView.createGradientLayer()
        self.profilePic.layer.cornerRadius = self.profilePic.frame.height / 2
        self.profilePic.layer.borderColor = UIColor.contactImageBorderColor.cgColor
        self.profilePic.layer.borderWidth = 3
        self.profilePic.layer.masksToBounds = true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
