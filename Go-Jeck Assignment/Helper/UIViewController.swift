//
//  UIViewController.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 17/03/19.
//  Copyright © 2019 Muvi. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    open var activityIndicator: UIActivityIndicatorView{
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .buttonColor
        return activityIndicator
    }
    
    public func indicatorStartAnimating(){
        let activity = activityIndicator
        self.view.addSubview(activity)
        activity.startAnimating()
    }
    
    public func indicatorStopAnimating(){
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.activityIndicator.removeFromSuperview()
    }
}
