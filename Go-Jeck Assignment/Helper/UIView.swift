//
//  UIView.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 16/03/19.
//  Copyright © 2019 Muvi. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    
    func createGradientLayer() {
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.bounds
        
        gradientLayer.colors = [UIColor(hex: "FFFFFF", alpha: 1.0).cgColor, UIColor(hex: "50E3C2", alpha: 0.5).cgColor]
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
