//
//  UIColor.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 15/03/19.
//  Copyright © 2019 Muvi. All rights reserved.
//

import Foundation
import UIKit
extension UIColor{
    
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            hexWithoutSymbol = String(hex[start...])
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.count) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            // TODO:ERROR
            break;
        }
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
    }
    
    class var buttonColor: UIColor{
        return UIColor(hex: "#50E3C2")
    }
    class var textColor: UIColor{
        return UIColor(hex: "#4A4A4A")
    }
    class var headerColor: UIColor{
        return UIColor(hex: "#4A4A4A", alpha: 0.5)
    }
    class var tableSeparatorColor: UIColor{
        return UIColor(hex: "#F0F0F0")
    }
    class var contactImageBorderColor: UIColor{
        return UIColor(hex: "#FFFFFF")
    }
}
