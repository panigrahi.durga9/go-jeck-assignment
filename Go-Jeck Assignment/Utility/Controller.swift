//
//  Controller.swift
//  Go-Jeck Assignment
//
//  Created by Muvi on 15/03/19.
//  Copyright © 2019 Muvi. All rights reserved.
//

import Foundation
class Controller{
  
    func getContactList(url : String, postCompleted : @escaping (_ succeeded: Bool, _ contacts: [ContactList]) -> ()) {
        var request = URLRequest(url: URL(string: url)!)
        let session = URLSession.shared
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
            
            guard let data = data else {
                postCompleted(false, [ContactList]())
                return
            }
            do {
                let contacts = try JSONDecoder().decode([ContactList].self, from: data)
                if contacts.count > 0{
                    postCompleted(true, contacts)
                }
            }
            catch {
                postCompleted(false, [ContactList]())
            }
        })
        task.resume()
    }
    
    func getContactDetails(url : String, postCompleted : @escaping (_ succeeded: Bool, _ contact: ContactDetails) -> ()) {
        var request = URLRequest(url: URL(string: url)!)
        let session = URLSession.shared
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
            var currentContactDetails: ContactDetails!
            guard let data = data else {
                postCompleted(false, currentContactDetails)
                return
            }
            do {
                currentContactDetails = try JSONDecoder().decode(ContactDetails.self, from: data)
                postCompleted(true, currentContactDetails)
            }
            catch {
                postCompleted(false, currentContactDetails)
            }
        })
        task.resume()
    }
   
    
    func editContactDetails(url : String,params:[String: String], postCompleted : @escaping (_ succeeded: Bool, _ contact: ContactDetails) -> ()) {
    
    // create post request
    let url = URL(string: url)!
    var request = URLRequest(url: url)
    request.httpMethod = "PUT"
    let session = URLSession.shared
    let jsonData = try? JSONSerialization.data(withJSONObject: params)
    request.httpBody = jsonData
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
        let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
            var currentContactDetails: ContactDetails!
            guard let data = data else {
                postCompleted(false, currentContactDetails)
                return
            }
            do {
                currentContactDetails = try JSONDecoder().decode(ContactDetails.self, from: data)
                postCompleted(true, currentContactDetails)
            }
            catch {
                postCompleted(false, currentContactDetails)
            }
        })
    task.resume()
    
    
    
    }
    
    func addContactDetails(url : String,params:[String: String], postCompleted : @escaping (_ succeeded: Bool, _ contact: ContactDetails) -> ()) {
        
        // create post request
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let session = URLSession.shared
        let jsonData = try? JSONSerialization.data(withJSONObject: params)
        request.httpBody = jsonData
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
            var currentContactDetails: ContactDetails!
            guard let data = data else {
                postCompleted(false, currentContactDetails)
                return
            }
            do {
                currentContactDetails = try JSONDecoder().decode(ContactDetails.self, from: data)
                postCompleted(true, currentContactDetails)
            }
            catch {
                postCompleted(false, currentContactDetails)
            }
        })
        task.resume()
        
        
        
    }
    
    
}
