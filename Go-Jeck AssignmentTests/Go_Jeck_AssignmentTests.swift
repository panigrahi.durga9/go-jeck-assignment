//
//  Go_Jeck_AssignmentTests.swift
//  Go-Jeck AssignmentTests
//
//  Created by Muvi on 15/03/19.
//  Copyright © 2019 Muvi. All rights reserved.
//

import XCTest
@testable import Go_Jeck_Assignment

class Go_Jeck_AssignmentTests: XCTestCase {

    let objectC = Controller()
    let appD = UIApplication.shared.delegate as! AppDelegate
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    func testGetContactList(){
        let expectation = self.expectation(description: "gotResult")
        self.objectC.getContactList(url: self.appD.appBaseURL +  "contacts.json") { (succeeded: Bool, contacts: [ContactList]) -> () in
            
            DispatchQueue.main.async(execute: {
                
                if(!succeeded){
                    XCTFail()
                }
                else
                {
                    XCTAssert(contacts.count > 0)
                    expectation.fulfill()
                }
            })
            
        }
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testGetContactDetails(){
        let expectation = self.expectation(description: "gotResult")
        self.objectC.getContactDetails(url: "http://gojek-contacts-app.herokuapp.com/contacts/4291.json") { (succeeded: Bool, contacts: ContactDetails) -> () in
            
            DispatchQueue.main.async(execute: {
                
                if(!succeeded){
                    XCTFail()
                }
                else
                {
                    XCTAssertEqual(contacts.first_name, "Durga", "gotResult")
                    expectation.fulfill()
                }
            })
            
        }
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
